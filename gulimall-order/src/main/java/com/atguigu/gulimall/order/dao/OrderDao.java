package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author steven
 * @email steven@gmail.com
 * @date 2022-01-09 09:47:03
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
