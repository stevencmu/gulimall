package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author steven
 * @email steven@gmail.com
 * @date 2022-01-09 09:58:42
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
