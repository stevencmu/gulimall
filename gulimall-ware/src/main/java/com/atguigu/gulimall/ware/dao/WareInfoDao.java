package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author steven
 * @email steven@gmail.com
 * @date 2022-01-09 10:18:54
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
