package com.atguigu.common.valid;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author Steven
 * @version 1.0
 * @date 2022/2/10 16:31
 * @description
 */
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {ListValueConstraintValidator.class})
public @interface ListValue {
    //默认会找validationMessage.properties
    String message() default "{com.atguigu.common.valid.ListValue.message}";
    Class<?>[] groups() default{ };
    // 可以指定数据只能为vals数组的值
    int[] vals() default {};
    Class<? extends Payload>[] payload() default { };
}
