package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 属性分组
 * 
 * @author steven
 * @email steven@gmail.com
 * @date 2022-01-08 16:31:09
 */
@Repository
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {

}
