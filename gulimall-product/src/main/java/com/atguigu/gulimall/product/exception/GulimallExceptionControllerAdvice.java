package com.atguigu.gulimall.product.exception;

import com.atguigu.common.exception.BizCodeEnume;
import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Steven
 * @version 1.0
 * @date 2022/2/10 16:03
 * @description
 */
@Slf4j
@RestControllerAdvice(basePackages = {"com.atguigu.gulimall.product.controller"})
public class GulimallExceptionControllerAdvice {

    /**
     * 参数校验失败处理
     * @param e 参数校验异常类型
     * @return 参数校验结果
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public R handleValidException(MethodArgumentNotValidException e){
        log.info("参数校验出错{},异常类型{}",e.getMessage(),e.getClass());
        //获取绑定结果
        BindingResult bindingResult = e.getBindingResult();
        Map<String,String> errorMap=new HashMap<>();
        bindingResult.getFieldErrors().forEach(item-> errorMap.put(item.getField(), item.getDefaultMessage()));
        return R.error(BizCodeEnume.VALID_EXCEPTION.getCode(),BizCodeEnume.VALID_EXCEPTION.getMsg()).put("data",errorMap );
    }

    /**
     * 处理所有异常
     * @param e 异常类型
     * @return 异常处理结果
     */
    @ExceptionHandler({Throwable.class})
    public R handleException(Throwable e){
        log.info("系统异常信息{}",e);
        return R.error(BizCodeEnume.UNKNOW_EXCEPTION.getCode(), BizCodeEnume.UNKNOW_EXCEPTION.getMsg());
    }
}
