package com.atguigu.gulimall.product.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Steven
 * @version 1.0
 * @date 2022/2/10 17:18
 * @description
 */
@Configuration
@MapperScan(basePackages = {"com.atguigu.gulimall.product.dao"})
public class MybatisConfig {

    //引入分页插件
    @Bean
    public PaginationInterceptor interceptor(){
        PaginationInterceptor interceptor=new PaginationInterceptor();
        // 设置请求页面的最大页数，true返回首页，false继续请求，默认为false
        interceptor.setOverflow(true);
        // 默认但也显示条数为500，-1条不受限制
        interceptor.setLimit(1000);
        // 开启count的join优化，只针对部分left join
        interceptor.setCountSqlParser(new JsqlParserCountOptimize(true));
        return interceptor;
    }
}
