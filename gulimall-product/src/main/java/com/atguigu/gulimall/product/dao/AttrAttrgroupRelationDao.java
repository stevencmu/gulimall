package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 属性&属性分组关联
 * 
 * @author steven
 * @email steven@gmail.com
 * @date 2022-01-08 16:31:09
 */
@Repository
@Mapper
public interface AttrAttrgroupRelationDao extends BaseMapper<AttrAttrgroupRelationEntity> {

    void deleteBatchRelation(@Param("vos") List<AttrAttrgroupRelationEntity> relationEntities);

    //void saveBatch(@Param("vos") List<AttrAttrgroupRelationEntity> relationEntityList);
}
