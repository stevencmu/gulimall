package com.atguigu.gulimall.product.service.impl;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.product.dao.SpuInfoDao;
import com.atguigu.gulimall.product.entity.SpuInfoEntity;
import com.atguigu.gulimall.product.service.SpuInfoService;
import com.atguigu.gulimall.product.vo.SpuSaveVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;


@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                new QueryWrapper<SpuInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSpuInfo(SpuSaveVo spuSaveVo) {
        // 1. 保存SPU基本信息 pms_sku_info

        // 2. 保存SPU的描述图片 pms_spu_info_desc

        // 3. 保存SPU的图片集 pms_spu_images

        // 4. 保存SPU的规格参数 pms_product_attr_value


        /*
         *  5   保存SPU对应的SKU信息
         *  5.1 保存SKU基本信息 pms_sku_info
         *  5.2 保存SKU图片信息 pms_sku_images
         *  5.3 保存SKU的销售属性值 pms_sku_sale_attr_value
         *  5.4 保存SKU的优惠、满减信息 gulimall-sms =>
         *      sms_sku_ladder、sms_sku_full_reduction、sms_member_price、
         *      sms_spu_bounds(保存积分信息)
         */



    }

}