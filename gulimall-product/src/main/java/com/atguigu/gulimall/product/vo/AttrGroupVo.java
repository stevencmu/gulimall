package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author Steven
 * @version 1.0
 * @date 2022/7/23 16:41
 * @description
 */
@Data
public class AttrGroupVo {
    private Long attrId;
    private Long attrGroupId;
}
