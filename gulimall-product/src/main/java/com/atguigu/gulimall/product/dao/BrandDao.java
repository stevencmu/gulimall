package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 品牌
 * 
 * @author steven
 * @email steven@gmail.com
 * @date 2022-01-08 16:31:09
 */
@Repository
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
