package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author Steven
 * @version 1.0
 * @date 2022/10/1 17:56
 * @description
 */
@Data
public class BrandVo {
    private Long brandId;
    private String brandName;
}
