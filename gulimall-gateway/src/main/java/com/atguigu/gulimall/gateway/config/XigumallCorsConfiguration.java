package com.atguigu.gulimall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * @author fuzq
 * @create
 */
@Configuration
public class XigumallCorsConfiguration {

    @Bean
    public CorsWebFilter corsWebFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        //所有跟跨域有关的配置都写在以下里面
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //配置跨域
        corsConfiguration.addAllowedHeader("*");//允许哪些头通过
        corsConfiguration.addAllowedMethod("*");//允许哪些请求方式进行跨域
        corsConfiguration.addAllowedOrigin("*");//允许哪些请求来源进行跨域
        corsConfiguration.setAllowCredentials(true);//是否允许携带cookie进行跨域
        source.registerCorsConfiguration("/**", corsConfiguration);//任意路径都进行，注册跨域的配置
        return new CorsWebFilter(source);
    }
}
